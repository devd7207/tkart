const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-pages-404-tsx": hot(preferDefault(require("/Users/tridevchaudhary/tkart/src/pages/404.tsx"))),
  "component---src-pages-checkout-js": hot(preferDefault(require("/Users/tridevchaudhary/tkart/src/pages/Checkout.js"))),
  "component---src-pages-index-tsx": hot(preferDefault(require("/Users/tridevchaudhary/tkart/src/pages/index.tsx")))
}

