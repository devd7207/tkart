require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}` // or '.env'
})

module.exports = {
  siteMetadata: {
    title: `Contentful Gatsby TKart`,
    description: `This is a static site e-commerce built with Contentful, Gatsby, and Commerce Layer`,
    author: ``
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Contentful Gatsby Commerce`,
        short_name: `commerce`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `standalone`,
        icon: `src/images/favicon.png` 
      }
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: 'thaer9xvdwnd',
        accessToken: 'kJE8D43Di6VaidhkqUqrc9fF5QyTgs7s_z1pRbIn7JA'
      }
    },
    `gatsby-plugin-commercelayer`,
    {
      resolve: `gatsby-plugin-typescript`,
      
      options: {
        isTSX: true,
        allExtensions: true
      }
    },
    `gatsby-plugin-sass`, 
    {
      resolve: 'gatsby-plugin-preconnect',
      options: {
        domains: [
          'https://marvel.commercelayer.io',
          'https://images.ctfassets.net'
        ]
      }
    },
    {
      resolve: "gatsby-plugin-segment-analytics",
      options: {
        writeKey: "NEXJeiuAJIqoFFmYCFAYJqGgji9bTlfS"
      }
    }
  ]
}
