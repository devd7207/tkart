import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import 'bulma'
import '../stylesheets/main.css'

const Checkout = () => {

  return (
    <React.Fragment>
      <Header
        shipping='US'
        lang='en-us'
        shoppingBagPreviewProps={{
          onClick: ''
        }}
      />
      <section id="main" >
      <div className="container">
        <br/>
      <h1 className="title"> Your order has been placed successfully!!</h1>
      </div>
      </section>
     
      <Footer />
      
    </React.Fragment>
  )
}


export default Checkout
