import React from 'react'
import PropTypes from 'prop-types'
import * as CLayer from 'commercelayer-react'
import Header from './Header'
import Footer from './Footer'
import 'bulma'
import '../stylesheets/main.css'
import ShoppingBag from './ShoppingBag'

const Layout = ({
  children,
  location,
  shoppingBagStatus,
  setShoppingBagStatus,
  ...props
}) => {
  const {
    pageContext: { shipping, language, marketId }
  } = props
  const sectionOpacity = shoppingBagStatus ? 'open' : ''
  return (
    <React.Fragment>
      <Header
        shipping={shipping}
        lang={language}
        shoppingBagPreviewProps={{
          onClick: setShoppingBagStatus
        }}
      />
      <section id="main" className={`section ${sectionOpacity}`}>
        <div className="container">{children}</div>
      </section>
      <Footer />
      <ShoppingBag
        lang={language}
        open={shoppingBagStatus}
        close={setShoppingBagStatus}
      />
      <CLayer.Config
        baseUrl="https://the-orange-brand-143.commercelayer.io"
        clientId="H9lJ1dPotDJ3S_dyLe_U9rDZmXUlswrKLMNe-XTL4CY"
        //client_secret="eyJhbGciOiJIUzUxMiJ9.eyJvcmdhbml6YXRpb24iOnsiaWQiOjIwNDV9LCJhcHBsaWNhdGlvbiI6eyJpZCI6Mjg5Mywia2luZCI6InNhbGVzX2NoYW5uZWwiLCJwdWJsaWMiOnRydWV9LCJ0ZXN0Ijp0cnVlLCJleHAiOjE2MDY5MTk1NzAsIm1hcmtldCI6eyJpZCI6MzU0OCwicHJpY2VfbGlzdF9pZCI6MzUyNCwic3RvY2tfbG9jYXRpb25faWRzIjpbMzkwNSwzOTA2XSwiZ2VvY29kZXJfaWQiOm51bGwsImFsbG93c19leHRlcm5hbF9wcmljZXMiOmZhbHNlfSwicmFuZCI6MC4wMDUxMTc3OTQxMDA4Mzc1Njl9.h2_Cg9T7d5XcquO8OIw-Y49VaH2yTDpPuWwYEn167wU4HyDeZdSmQYEaKGl2lf9__Rd6xB72xeWnngtlVh68sQ"
        marketId="3548"
        countryCode={shipping ? shipping.toUpperCase() : 'US'}
        languageCode={
          language ? language.toLowerCase().replace('-us', '') : 'en'
        }
      />
    </React.Fragment>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

Layout.defaultProps = {
  location: {
    pathname: ''
  }
}

export default Layout
