"use strict";

/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
// You can delete this file if you're not using it
var path = require("path");

exports.createPages = function _callee(_ref) {
  var graphql, actions, countryBuild, languagesBuild, env, createPage, result, edges;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          graphql = _ref.graphql, actions = _ref.actions;
          countryBuild = process.env.GATSBY_COUNTRY_BUILD;
          languagesBuild = process.env.GATSBY_COUNTRY_LANGUAGES;
          console.log('Build by Country: ', countryBuild);
          console.log('Languages: ', languagesBuild);
          env = process.env.NODE_ENV;
          createPage = actions.createPage;

          if (!(countryBuild === 'none')) {
            _context.next = 9;
            break;
          }

          return _context.abrupt("return", null);

        case 9:
          _context.next = 11;
          return regeneratorRuntime.awrap(graphql("\n    query {\n      allContentfulCountry {\n        edges {\n          node {\n            node_locale\n            code\n            market_id\n            catalogue {\n              name\n              node_locale\n              categories {\n                name\n                products {\n                  name\n                  contentful_id\n                }\n                contentful_id\n                products_ca {\n                  name\n                  contentful_id\n                }\n                products_cn {\n                  name\n                  contentful_id\n                }\n                products_gb {\n                  name\n                  contentful_id\n                }\n                products_it {\n                  name\n                  contentful_id\n                }\n                products_jp {\n                  name\n                  contentful_id\n                }\n              }\n            }\n          }\n        }\n      }\n    }\n  "));

        case 11:
          result = _context.sent;
          edges = result.data.allContentfulCountry.edges.filter(function (_ref2) {
            var node = _ref2.node;
            return languagesBuild.search(node.node_locale) !== -1;
          });
          edges.forEach(function (_ref4) {
            var node = _ref4.node;
            var code = node.code.toLowerCase();
            var locale = node.node_locale.toLowerCase();
            if (languagesBuild.toLocaleLowerCase().search(locale) === -1) return null; // Catalogue page

            var cataloguePath = "/".concat(code, "/").concat(locale, "/") ;
            console.log('cataloguePath :', cataloguePath);
            createPage({
              path: cataloguePath,
              component: path.resolve("./src/templates/CatalogPage.tsx"),
              context: {
                // Data passed to context is available in page queries as GraphQL variables.
                slug: cataloguePath,
                language: node.node_locale,
                shipping: node.code,
                pageTitle: node.node_locale === 'it' ? 'Categorie' : 'Categories',
                marketId: node.market_id
              }
            });
            node.catalogue.categories.map(function (c) {
              // Category page
              var categorySlug = c.name.trim().toLowerCase().replace(' & ', ' ').replace(/\s/gm, '-');
              var categoryPath = "/".concat(code, "/").concat(locale, "/").concat(categorySlug) ;
              console.log('categoryPath :', categoryPath);
              createPage({
                path: categoryPath,
                component: path.resolve("./src/templates/CategoryPage.tsx"),
                context: {
                  // Data passed to context is available in page queries as GraphQL variables.
                  slug: categoryPath,
                  language: node.node_locale,
                  shipping: node.code,
                  categoryId: c.contentful_id,
                  categorySlug: categorySlug,
                  pageTitle: c.name.trim(),
                  marketId: node.market_id
                }
              });
              var products = c["products_".concat(code)] ? c["products_".concat(code)] : c.products;
              products.map(function (p) {
                var productSlug = p.name.trim().toLowerCase().replace(/\s/gm, '-');
                var productPath =  "/".concat(code, "/").concat(locale, "/").concat(categorySlug, "/").concat(productSlug);
                console.log('productPath :', productPath); // Product

                createPage({
                  path: productPath,
                  component: path.resolve("./src/templates/ProductPage.tsx"),
                  context: {
                    // Data passed to context is available in page queries as GraphQL variables.
                    slug: productPath,
                    language: node.node_locale,
                    shipping: node.code,
                    categoryId: c.contentful_id,
                    categorySlug: categorySlug,
                    categoryName: c.name.trim(),
                    productId: p.contentful_id,
                    pageTitle: p.name.trim(),
                    marketId: node.market_id
                  }
                });
              });
            });
          });

        case 14:
        case "end":
          return _context.stop();
      }
    }
  });
};